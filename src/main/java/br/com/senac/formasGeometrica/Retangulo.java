/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.formasGeometrica;

import br.com.senac.geometrico.FiguraGeometrico;


public class Retangulo extends FiguraGeometrico{
    int lado;
    int largura;

    public Retangulo(int lado, int largura) {
        this.lado = lado;
        this.largura = largura;
    }
        
    
    @Override
    public int getareaG() {
        
        
   int resultado;
   resultado = this.lado*this.largura;
        
        
        
    return resultado;}
    
}
