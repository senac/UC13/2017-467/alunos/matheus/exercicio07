/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.formasGeometrica;

import br.com.senac.geometrico.FiguraGeometrico;

/**
 *
 * @author sala304b
 */
public class Circulo extends FiguraGeometrico{
    
    int raio;
    int PI = (int) 3.1415;
    
    public Circulo (int raio){
        this.raio = raio;
    }
    
    
    
    @Override
    public int getareaG() {
       int resultado;
       resultado = this.raio*PI;
        
    return resultado;}
    
}
