/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.formasGeometrica;

import br.com.senac.geometrico.FiguraGeometrico;


public class Triangulo extends FiguraGeometrico{
    
    int base;
    int altura;

    public Triangulo(int base, int altura) {
        this.base = base;
        this.altura = altura;
    }
    
    @Override
    public int getareaG() {
        int resultado;
        
        resultado = (this.base * this.altura)/2;
    return resultado;}
}
