/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.teste;

import br.com.senac.formasGeometrica.Circulo;
import br.com.senac.formasGeometrica.Quadrado;
import br.com.senac.formasGeometrica.Retangulo;
import br.com.senac.formasGeometrica.Triangulo;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class TesteGeometrico {
    
    public TesteGeometrico() {
    }
    
    @Test
    public void TemqueretornaAreaQuadrado (){
        Quadrado quadrado =  new Quadrado (30);
        
        assertEquals(900, quadrado.getareaG(), 0.02);
    }
    
    @Test
    public void TemqueretornaAreaRetangulo (){
        Retangulo retangulo = new Retangulo (30, 40);
        
        assertEquals(1200, retangulo.getareaG(), 0.02);
    }
    
    @Test
    public void TemqueretornaAreaCirculo (){
        Circulo circulo = new Circulo (6);
        
        assertEquals(18.849, circulo.getareaG(), 0.2);
    }
    
    @Test
    public void TemqueretornaAreaTriangulo (){
        Triangulo triangulo = new Triangulo (8,9);
        assertEquals(36, triangulo.getareaG(), 0.2);
    }
}
